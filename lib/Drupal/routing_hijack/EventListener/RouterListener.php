<?php
/**
 * @file
 * Contains Drupal\routing_hijack\EventListener\RouterListener.
 */

namespace Drupal\routing_hijack\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;

class RouterListener implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return array(KernelEvents::REQUEST => array('onKernelRequest', 33));
  }

  public function onKernelRequest(GetResponseEvent $event) {
    $request = $event->getRequest();
    $path = $request->getPathInfo();
    $path = trim($path, '/');
    list($first) = explode('/', $path);
    if (!in_array($first, array('admin', 'contextual', 'toolbar'))) {
      $response = new Response('this is a test');
      $event->setResponse($response);
    }
  }

}